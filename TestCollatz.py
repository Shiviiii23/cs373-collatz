#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(250, 3500)
        self.assertEqual(v, 217)

    def test_eval_2(self):
        v = collatz_eval(3000, 3500)
        self.assertEqual(v, 199)

    def test_eval_3(self):
        v = collatz_eval(1250, 420)
        self.assertEqual(v, 182)

    def test_eval_4(self):
        v = collatz_eval(7894, 7839)
        self.assertEqual(v, 190)

    def test_eval_5(self):
        v = collatz_eval(4563, 90)
        self.assertEqual(v, 238)

    def test_eval_6(self):
        v = collatz_eval(1, 90000)
        self.assertEqual(v, 351)

    def test_eval_7(self):
        v = collatz_eval(70, 1078)
        self.assertEqual(v, 179)

    def test_eval_8(self):
        v = collatz_eval(890, 1090)
        self.assertEqual(v, 174)

    def test_eval_9(self):
        v = collatz_eval(678, 90002)
        self.assertEqual(v, 351)

    def test_eval_10(self):
        v = collatz_eval(89, 67829)
        self.assertEqual(v, 340)

    def test_eval_11(self):
        v = collatz_eval(999, 2999)
        self.assertEqual(v, 217)
    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
